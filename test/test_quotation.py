from bztogl import template

def _compare_quotations(prefix):
    with open('test/data/{}-input.txt'.format(prefix)) as f:
        raw = f.read()

    with open('test/data/{}-expected.txt'.format(prefix)) as f:
        expected = f.read()

    actual = template.render_comment('', 'speech_balloon', 'Andre Miranda', 'said', raw, '')
    assert actual.strip() == expected

def test_comment_separated_from_quotation():
    _compare_quotations('quotation1')
